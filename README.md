# FTH API

## Getting started

1. Prerequisite:

- Server: Ubuntu Server 20.04
- Install Docker: https://docs.docker.com/engine/install/ubuntu/
- Install Docker Compose: https://docs.docker.com/compose/install/

2. Build & Start API:
   docker-compose up -d

3. Run migrate:
   docker-compose exec api python manage.py migrate

4. Create Admin:
   docker-compose exec api python manage.py create_admin

5. Account admin default:

- Username: admin@nng.bz
- Password: 123123

6. Check docker status:
   docker ps

7. Check docker logs:
   docker-compose logs -tf --tail 200

8. Run unit test:
   docker-compose exec api pytest
