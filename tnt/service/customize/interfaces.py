from graphene import relay


class CustomizeInterface(relay.Node):
    class Meta:
        name = "CustomizeInterface"

    @classmethod
    def to_global_id(cls, type, id):
        return id