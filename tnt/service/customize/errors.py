import graphene


def errors(message, code, field=None):
    return [
        {
            "message": message,
            "field": field,
            "code": code,
        }
    ]


class ErrorType(graphene.ObjectType):
    message = graphene.String()
    code = graphene.String()
    field = graphene.String(default_value=None)
