from graphql_jwt.decorators import user_passes_test
# from tdl.users.models import UserType


def permission_required(perm):
    def check_perms(user):
        if isinstance(perm, str):
            perms = (perm,)
        else:
            perms = perm
        # if user.is_anonymous or user.user_type == UserType.CLIENT or user.user_type == UserType.EMPLOYEE:
        #     return True
        return user.has_perms(perms)
    return user_passes_test(check_perms)
