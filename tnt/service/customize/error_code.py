from django.db import models


class UserError(models.TextChoices):
    USER_01 = "You do not have permission to perform this action"
    USER_02 = "Email already exist"
    USER_03 = "Password must be at least 6 characters"
    USER_04 = "User does not exist"
    USER_05 = "Email does not exist"
    USER_06 = "Code invalid or expired time"
    USER_07 = "Confirm password does not match"
    USER_08 = "Email or Password does not match"
    USER_09 = "Password is incorrect"
    USER_10 = "The email or password is incorrect"
    USER_11 = "The account have been locked because login fail over three times."
    USER_12 = "Password does not match"
    USER_13 = "Username already exist"

class DeviceError(models.TextChoices):
    DEV_01 = "Device already exist"
    DEV_02 = "Device does not exist"
    DEV_03 = "Parameter already exist"
    DEV_04 = "Parameter does not exist"
    DEV_05 = "Parameter can not set the same tag"
    DEV_06 = "Image does not exist"
    DEV_07 = "Control param must require type"
    DEV_08 = "Group parameter name already exist"
    DEV_09 = "Group parameter name does not exist"
    DEV_10 = "Value input of control parameter is interger. Please input correct field."
    DEV_11 = "Value input of control parameter is float. Please input correct field."
    DEV_12 = "Value input of control parameter is boolean. Please input correct field."
    DEV_13 = "Analytics already exist"
    DEV_14 = "Analytics does not exist"
class AlarmError(models.TextChoices):
    ALA_01 = "Alarm does not exist"
    ALA_02 = "Please input correct field"

class OrganizationError(models.TextChoices):
    ORN_01 = "Organization does not exist"
    ORN_02 = "Organization name already exist"
    ORN_03 = "Sub group does not exist"
    ORN_04 = "Please input organization"

class MaintenanceError(models.TextChoices):
    MAIN_01 = "Maintenance template does not exist"
    MAIN_02 = "Maintenance template form does not exist"
    MAIN_03 = "Maintenance template form field does not exist"
    MAIN_04 = "Maintenace does not exist"
    MAIN_05 = "This device have already maintenance"
    MAIN_06 = "You do not have permission to perform this action"