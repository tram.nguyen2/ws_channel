# Generated by Django 3.1.5 on 2021-11-01 09:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20210909_1105'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='is_locked',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='user',
            name='login_fail_time',
            field=models.IntegerField(default=0),
        ),
    ]
