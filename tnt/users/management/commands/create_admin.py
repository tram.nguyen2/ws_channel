from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.hashers import make_password

from tnt.users.models import User, UserType


class Command(BaseCommand):
    def create(self):
        try:
            user, user_created = User.objects.get_or_create(
                email="admin@gmail.com", user_type=UserType.SUPERADMIN, is_active=True, is_superuser=True, is_staff=True
            )
            if user_created:
                user.set_password("123123")
                user.save()
            print("----------------------> Create master admin successful")
        except Exception as error:
            print("----------------------> Error", error)

    def handle(self, *args, **options):
        print("----------------------> Loading")
        self.create()
