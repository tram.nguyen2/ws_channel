from enum import auto
import os
import uuid
import binascii
import random

from django.conf import settings
from django.db import models
from django.contrib.auth.models import BaseUserManager, PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.db.models.fields import DateTimeField
from django.utils.translation import gettext_lazy as _
from model_utils.models import TimeStampedModel


# def user_avatar_directory_path(instance, filename):
#     return os.path.join("user_avatar", str(instance.username), "{}.{}".format(uuid.uuid4(), filename.split(".")[-1]))


class UserType(models.TextChoices):
    SUPERADMIN = "super_admin", _("super_admin")
    ADMIN = "admin", _("admin")
    ADMIN_GROUP = "admin_group", _("admin_group")
    USER = "user", _("user")


class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(email, password, **extra_fields)


class AbstractUser(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        error_messages={'unique': _("A user with that username already exists."), },
        blank=True,
        null=True,
    )
    email = models.EmailField(_('email address'), blank=True)
    is_staff = models.BooleanField(_("staff status"), default=False,)
    is_active = models.BooleanField(_("active"), default=False,)
    is_superuser = models.BooleanField(_("superuser"), default=False,)
    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'

    class Meta:
        abstract = True


class User(AbstractUser, TimeStampedModel):
    email = models.EmailField(unique=True)
    username = models.CharField(max_length=50, unique=True, null=True, blank=True)
    user_type = models.CharField(max_length=50, choices=UserType.choices, null=True, blank=True)
    last_name = models.CharField(max_length=50, null=True, blank=True)
    first_name = models.CharField(max_length=50, null=True, blank=True)
    date_of_birth = models.DateTimeField(null=True, blank=True)
    phone = models.CharField(max_length=50, null=True, blank=True)
    activate_token = models.CharField(max_length=50, null=True, blank=True)
    activate_time = models.DateTimeField(null=True, blank=True)
    change_password_token = models.CharField(max_length=50, null=True, blank=True)
    change_password_time = models.DateTimeField(null=True, blank=True)
    login_fail_time = models.IntegerField(default=0)
    login_fail_timeline = models.DateTimeField(null=True, blank=True)
    is_locked = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if not self.id:
            self.activate_token = self.generate_key()

        return super().save(*args, **kwargs)

    def generate_key(self):
        return "".join(random.choice("0123456789") for i in range(6))

    def is_admin(self):
        if self.user_type == UserType.SUPERADMIN:
            return True
        return False

    def __str__(self):
        return self.email

    class Meta:
        db_table = "users_user"
