from django.conf import settings
from django.utils import timezone
from celery import Celery
from tnt.users.models import User
app = Celery("tnt", broker=settings.RABBITMQ_URL)

@app.task
def check_user():
    try:
        users = User.objects.filter(login_fail_time__range = [1,5])
        for user in users:
            time = timezone.now() - user.login_fail_timeline
            day = time.days
            hours, remainder = divmod(time.seconds, 3600)
            minutes, seconds = divmod(remainder, 60)
            print("minutes: ", minutes)
            if minutes >= 2:
                user.login_fail_time = 0
                user.is_locked=False
                user.save()

        print("Update successfully")
    except Exception as error:
        print("--------> error: ", error)