from django.urls import re_path

from tnt.blog.consumers import BlogConsumer, ChatConsumer

websocket_urlpatterns = [
    re_path(r'ws/blog/', BlogConsumer.as_asgi()),
    re_path(r'ws/chat/', ChatConsumer.as_asgi()),
]