from django.db import models
from model_utils.models import TimeStampedModel
from tnt.users.models import User
# Create your models here.
class Blog(TimeStampedModel,models.Model):
    title = models.CharField(max_length=200)
    content=models.TextField(null=True,blank=True)
    created_by=models.ForeignKey(User, on_delete=models.CASCADE, null=True,blank=True)
    
    class Meta:
        db_table = "blog"

class Chat(TimeStampedModel,models.Model):
    message=models.TextField(null=True,blank=True)
    created_by=models.ForeignKey(User, on_delete=models.CASCADE, null=True,blank=True)
    
    class Meta:
        db_table = "chat" 
