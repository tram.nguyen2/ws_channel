
import json

from django.utils import timezone
from channels.generic.websocket import WebsocketConsumer
from asgiref.sync import async_to_sync
from tnt.blog.models import Blog, Chat
from firebase_admin.messaging import Message, Notification
from fcm_django.models import FCMDevice
from firebase_admin import initialize_app
from channels.db import database_sync_to_async
from tnt.users.models import User
from django.contrib.auth.models import AnonymousUser

# @database_sync_to_async
# def get_user(user_id):
#     try:
#         return User.objects.get(id=user_id)
#     except User.DoesNotExist:
#         return AnonymousUser()


initialize_app()



class ChatConsumer(WebsocketConsumer):

    def connect(self):
        self.room_group_name = 'test'
        self.user = self.scope["user"]
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )
        

        self.accept()
   

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        

        user=self.user


        FCMDevice.objects.send_message(
        Message(notification=Notification(title="Send From ChatApp", body=message))
        )
        if not user.is_anonymous:
            Chat.objects.create(message=message,created_by=user)

        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type':'chat_message',
                'message':message,
                'user':'' if user.is_anonymous else user
            }
        )

    def chat_message(self, event):
        message = event['message']

        self.send(text_data=json.dumps({
            'type':'chat',
            'message':message,
            'user':'' if self.user.is_anonymous else self.user
        }))


class BlogConsumer(WebsocketConsumer):

    def connect(self):
        self.room_group_name = 'blog'

        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )
        self.accept()
   

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        title = text_data_json['title']
        content=text_data_json['content']

        # user = info.context.user
        Blog.objects.create(title=title,content=content)

        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type':'blog_message',
                'title':title,
                'content':content
            }
        )

    def blog_message(self, event):
        title = event['title']
        content=event['content']

        self.send(text_data=json.dumps({
            'type':'blog',
            'title':title,
            'content':content
        }))


