from django import views
from django.urls import path

from . import views


urlpatterns = [
    path('blog/', views.blog),
    path('createBlog/',views.addBlog)
]