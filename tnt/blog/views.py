from audioop import reverse
from django.shortcuts import redirect, render
from django.contrib.auth.forms import  UserCreationForm

from tnt.blog.models import Blog

# Create your views here.
def blog(request):
    blogs=Blog.objects.all()
    return render(request, 'blog/blog.html',{'blogs':blogs})

def addBlog(request):
    return render(request, 'blog/addBlog.html')
