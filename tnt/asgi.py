"""
ASGI config for tnt project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""

import os
from channels.auth import AuthMiddlewareStack
from django.core.asgi import get_asgi_application
from channels.http import AsgiHandler
from channels.routing import ProtocolTypeRouter,URLRouter
import tnt.blog.routing as routing
from django.conf import settings
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tnt.settings')
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.path.join('tnt/google.json')

# application = get_asgi_application()


application = ProtocolTypeRouter({
  "http": get_asgi_application(),
    "websocket": AuthMiddlewareStack(
        URLRouter(
            routing.websocket_urlpatterns
        )
    ),
})
