import graphene

from tnt.service.customize.errors import ErrorType, errors
from django.core.management import call_command
from django.db import transaction


class MigrationInput(graphene.InputObjectType):
    accept = graphene.Boolean(default_value=False)
    migrate = graphene.Boolean(default_value=False)
    migrate_mongo = graphene.Boolean(default_value=False)
    accept_mongo = graphene.Boolean(default_value=False)
    create_admin = graphene.Boolean(default_value=False)


class Migration(graphene.Mutation):
    status = graphene.Boolean(default_value=False)
    errors = graphene.List(ErrorType)

    class Arguments:
        input = MigrationInput(required=True)

    def mutate(root, info, input):
        try:
            if input.accept:
                call_command('flush', '--noinput')

            if input.migrate:
                call_command("migrate")

            if input.migrate_mongo:
                call_command("migrate", database="mongo")

            if input.create_admin:
                call_command("create_admin")
            
            if input.accept_mongo:
                call_command('flush', '--noinput', database="mongo")

            return Migration(status=True)

        except Exception as error:
            transaction.set_rollback(True)
            return Migration(error)


class Mutation(graphene.ObjectType):
    migration = Migration.Field()
