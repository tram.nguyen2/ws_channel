from graphene_django import DjangoObjectType
import graphene_django_optimizer as gql_optimizer
from tnt.blog.models import Chat
from tnt.service.customize.connection import CustomizeFilterConnectionField
from tnt.service.customize.interfaces import CustomizeInterface
from tnt.service.graphene_django_plus.fields import CountableConnection
import graphene
from django_filters import FilterSet,OrderingFilter
class ChatFilter(FilterSet):
    class Meta:
        model = Chat
        fields = ["message","created_by"]

    order_by = OrderingFilter(fields=({"id": "id"}))

class ChatNode(DjangoObjectType):
    class Meta:
        model = Chat
        interfaces = (CustomizeInterface,)
        only_fields = ["message","created_by"]
        connection_class = CountableConnection
        filterset_class=ChatFilter
    @classmethod
    def get_queryset(cls, queryset, info):
        return gql_optimizer.query(queryset, info)

class Query(graphene.ObjectType):
    chats=CustomizeFilterConnectionField(ChatNode)