import graphene
import django_filters
from graphene.types import field, interface
import graphene_django_optimizer as gql_optimizer

from graphql_jwt.decorators import superuser_required, login_required
from graphene_django import DjangoObjectType

from django_filters import FilterSet, Filter, OrderingFilter, CharFilter, BooleanFilter
from tnt.users.models import User
from tnt.service.customize.interfaces import CustomizeInterface
from tnt.service.graphene_django_plus.fields import CountableConnection
from tnt.service.customize.connection import CustomizeFilterConnectionField

from django.db.models import Q,Value as V
from django.db.models.functions import Concat

class UserFilter(FilterSet):
    user_type = CharFilter(field_name="user_type", lookup_expr="exact")
    phone = CharFilter(field_name="phone", lookup_expr="icontains")
    email = CharFilter(field_name="email", lookup_expr="icontains")
    name = CharFilter(method="filter_by_name")
    class Meta:
        model = User
        fields = []

    order_by = OrderingFilter(fields=({"id": "id"}))

    def filter_by_name(self, queryset, name, value):
        return queryset.annotate(name=Concat('first_name', V(' '), 'last_name')).filter(Q(first_name__icontains=value) |
                                                                                                                Q(last_name__icontains=value) | Q(name__icontains=value))


class UserNode(DjangoObjectType):
    class Meta:
        model = User
        interfaces = (CustomizeInterface,)
        only_fields = ["id", "username", "last_name", "first_name", "email", "phone", "user_type","date_of_birth", "is_locked", "login_fail_time"]
        filterset_class = UserFilter
        connection_class = CountableConnection

    @classmethod
    def get_queryset(cls, queryset, info):
        return gql_optimizer.query(queryset.exclude(Q(user_type='super_admin') | Q(user_type__isnull=True)), info)


class Query(graphene.ObjectType):
    user_profile = graphene.Field(UserNode)
    user_detail = graphene.Field(UserNode, id=graphene.String(required=True))

    users_list = CustomizeFilterConnectionField(UserNode)

    @login_required
    def resolve_user_profile(root, info):
        return info.context.user

    @login_required
    def resolve_user_detail(root, info, id, **kwargs):
        return User.objects.filter(id=id).first()
