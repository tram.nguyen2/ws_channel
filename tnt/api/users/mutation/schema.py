from graphql_jwt.decorators import login_required
import graphene
import graphql_jwt
import os
import random
from graphene_django import DjangoObjectType
from graphene_file_upload.scalars import Upload
from graphql_auth import relay
# from graphene_django_plus.mutations import ModelCreateMutation, ModelUpdateMutation, ModelDeleteMutation
from graphene.relay.node import to_global_id
from graphql_jwt.decorators import superuser_required, login_required
from graphql_jwt.settings import jwt_settings
from tnt.service.graphene_django_plus.mutations import ModelCreateMutation, ModelUpdateMutation, ModelDeleteMutation

from django.core.mail import send_mail
from django.db import transaction, models
from django.utils import timezone
from django.conf import settings
from django.contrib.auth.hashers import check_password
from django.contrib.auth import authenticate
from django.core.management import call_command
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model

from tnt.users.models import User
from tnt.api.users.query.schema import UserNode

from tnt.service.customize.errors import ErrorType, errors
from tnt.service.customize.error_code import UserError

from graphql_jwt.decorators import token_auth
from django.contrib.auth.backends import ModelBackend
from fcm_django.models import FCMDevice
UserModel = get_user_model()



class AuthencationLogin(graphql_jwt.relay.JSONWebTokenMutation):
    status = graphene.Boolean(default_value=False)
    errors = graphene.List(ErrorType)
    user = graphene.Field(UserNode)

    @classmethod
    def mutate(cls, root, info, input):
        user = authenticate(info.context, username=input.email, password=input.password)
        if user is None:
            return cls(errors=errors(UserError.USER_08, "USER_08", "password"))
        
        return super().mutate(root, info, input)

    @classmethod
    def resolve(cls, root, info, **kwargs):
        user=info.context.user
        user_device=FCMDevice.objects.filter(user_id=user.id).first()
        if user_device:
            user_device.active=True
            user_device.save()
        return cls(user=user, status=True)


class AdminCreateUser(ModelCreateMutation):

    class Meta:
        model = User
        only_fields = ["username", "email", "password", "user_type", "first_name","last_name","phone","date_of_birth"]
        required_fields = ["username", "email", "password", "user_type"]

    @classmethod
    @superuser_required
    def mutate(cls, root, info, input):
        if User.objects.filter(username=input["username"]).exists():
            return cls(errors=errors(UserError.USER_13, "USER_13", "username"))
            
        if User.objects.filter(email=input["email"]).exists():
            return cls(errors=errors(UserError.USER_02, "USER_02", "email"))

        if len(input["password"]) < 6:
            return cls(errors=errors(UserError.USER_03, "USER_03", "password"))
        cls.Input._meta.fields.update({"user": graphene.Field(UserNode)})
        return super().mutate(root, info, input)

    @classmethod
    def perform_mutation(cls, root, info, **data):
        payload = super().perform_mutation(root, info, **data)
        payload.user.set_password(data.get("password"))
        payload.user.is_active = True
        payload.user.save()
        return cls(status=True, user=payload.user)


class AdminUpdateUser(ModelUpdateMutation):
    class Meta:
        model = User
        only_fields = ["username", "user_type", "first_name", "is_locked","last_name","phone","date_of_birth","email"]

    class Input:
        id=graphene.String()
        new_password = graphene.String()
        confirm_password = graphene.String()

    @classmethod
    def mutate(cls, root, info, input):
        if not User.objects.filter(id=input["id"]).exists():
            return cls(errors=errors(UserError.USER_04, "USER_04", "id"))
        
        if "new_password" in input and "confirm_password" in input and input["new_password"] != input["confirm_password"]:
            return cls(errors=errors(UserError.USER_07, "USER_07", "id"))

        if "email" in input and User.objects.filter(email=input["email"]).exclude(id=input.id).exists():
            return cls(errors=errors(UserError.USER_02, "USER_02", "email"))
        
        if "username" in input and User.objects.filter(username=input["username"]).exclude(id=input.id).exists():
            return cls(errors=errors(UserError.USER_13, "USER_13", "username"))

        cls.Input._meta.fields.update({"user": graphene.Field(UserNode)})
        return super().mutate(root, info, input)

    @classmethod
    def perform_mutation(cls, root, info, **data):
        payload = super().perform_mutation(root, info, **data)
        if "is_locked" in data and not data["is_locked"]:
            payload.user.login_fail_time = 0
        if "new_password" in data and data["new_password"] != "" and data["new_password"] is not None:
            payload.user.set_password(data.get("new_password"))
        payload.user.save()
        return cls(status=True, user=payload.user)


class ForgotPasswordInput(graphene.InputObjectType):
    email = graphene.String(required=True)


class ForgotPasswordToken(graphene.Mutation):
    status = graphene.Boolean(default_value=False)
    errors = graphene.List(ErrorType)

    class Arguments:
        input = ForgotPasswordInput(required=True)

    def mutate(root, info, input):
        user = User.objects.filter(email=input["email"], is_active=True).first()
        if user != None:
            token = "".join(random.choice("0123456789") for i in range(6))
            user.change_password_token = token
            user.change_password_time = timezone.now() + timezone.timedelta(minutes=10)
            user.save()
            content = f"""<h2>Forgot Password!</h2>
                <p>Your token: {user.change_password_token}</p>
                <p>Code is valid for 10 minutes</p>
                """
            send_mail("FTH - Forgot password", content, "FTH <noreply@nng.bz>",
                      [user.email], html_message=content, fail_silently=True)
            return ForgotPasswordToken(status=True)
        return ForgotPasswordToken(errors=[{"message": UserError.USER_05, "field": "email", "code": "USER_05"}])


class TokenVerifyInput(graphene.InputObjectType):
    code = graphene.String(required=True)


class ForgotPasswordVerify(graphene.Mutation):
    status = graphene.Boolean(default_value=False)
    errors = graphene.List(ErrorType)

    class Arguments:
        input = TokenVerifyInput(required=True)

    def mutate(root, info, input):
        user = User.objects.filter(change_password_token=input["code"]).first()
        if user != None and user.change_password_time > timezone.now():
            return ForgotPasswordVerify(status=True)
        return ForgotPasswordVerify(errors=[{"message": UserError.USER_06, "field": "code", "code": "USER_06"}])


class CreateNewPasswordInput(graphene.InputObjectType):
    code = graphene.String(required=True)
    password = graphene.String(required=True)
    confirm_password = graphene.String(required=True)


class CreateNewPassword(graphene.Mutation):
    status = graphene.Boolean(default_value=False)
    errors = graphene.List(ErrorType)

    class Arguments:
        input = CreateNewPasswordInput(required=True)

    def mutate(root, info, input):
        try:
            if len(input["password"]) < 6:
                return CreateNewPassword(errors=[{"message": UserError.USER_03, "field": "password", "code": "USER_03"}])

            if input["password"] != input["confirm_password"]:
                return CreateNewPassword(errors=[{"message": UserError.USER_07, "field": "confirm_password", "code": "USER_07"}])

            user = User.objects.filter(change_password_token=input["code"]).first()
            if user is None:
                return CreateNewPassword(errors=[{"message": UserError.USER_06, "field": "code", "code": "AUTH_06"}])

            if user != None:
                user.set_password(input["password"])
                user.save()
                return CreateNewPassword(status=True)
        except Exception as error:
            return CreateNewPassword(error)


class ChangeProfileInput(graphene.InputObjectType):
    first_name = graphene.String()
    last_name = graphene.String()
    phone = graphene.String()
    date_of_birth = graphene.DateTime()

class ChangeProfile(graphene.Mutation):
    status = graphene.Boolean(default_value=False)
    errors = graphene.List(ErrorType)
    user = graphene.Field(UserNode)

    class Arguments:
        input = ChangeProfileInput()

    def mutate(root, info, input):
        if info.context.user.is_anonymous:
            return ChangeProfile(errors=[{"message": UserError.USER_01, "field": None, "code": "USER_01"}])

        user = info.context.user
        for key, value in input.items():
            if key in [f.name for f in User._meta.get_fields()]:
                setattr(user, key, value)
        user.save()
        return ChangeProfile(status=True, user=user)


class JSONWebTokenMutation(graphql_jwt.mixins.ObtainJSONWebTokenMixin,
                           graphene.ClientIDMutation):

    class Meta:
        abstract = True

    @classmethod
    def Field(cls, *args, **kwargs):
        cls._meta.arguments['input']._meta.fields.update({
            get_user_model().USERNAME_FIELD:
            graphene.InputField(graphene.String, required=True),
            'password': graphene.InputField(graphene.String, required=True),
        })
        return super().Field(*args, **kwargs)

    @classmethod
    @token_auth
    def mutate_and_get_payload(cls, root, info, **kwargs):
        return cls.resolve(root, info, **kwargs)


class CustomModelBackend(ModelBackend):
    def authenticate(self, request, email=None, password=None, user_type=None, **kwargs):
        if email is None or password is None:
            return
        data = User.objects.filter(email=email).first()
        if data:
            email = data.email
        else:
            return None
        try:
            user = User.objects.filter(email=email).first()
            if not user:
                return None
        except UserModel.DoesNotExist:
            UserModel().set_password(password)
        else:
            if user.check_password(password) and ModelBackend.user_can_authenticate(self, user):
                return user
            else:
                _user = User.objects.filter(email=email).first()
                if _user.user_type == 'super_admin':
                    return None
                if _user.login_fail_time < 5:
                    _user.login_fail_time += 1
                    _user.login_fail_timeline = timezone.now()
                    _user.save()
                    return None
                if _user.login_fail_time == 5:
                    _user.is_locked = True
                    _user.save()
                    return 'locked'


class Login(JSONWebTokenMutation):
    status = graphene.Boolean(default_value=False)
    errors = graphene.List(ErrorType)
    user = graphene.Field(UserNode)
    fail_time = graphene.Int(default_value=0)

    @classmethod
    def mutate(cls, root, info, input):
        username = input.email
        if User.objects.filter(username = username).exists():
            username = User.objects.filter(username = username).first().email
        user = CustomModelBackend.authenticate(cls, info.context, email=username, password=input.password)

        if user is None:
            user_fail = User.objects.filter(email=username).first()
            if user_fail is not None:
                return Login(errors=[{"message": "The email or password is incorrect", "field": "email", "code": "USER_10"}], fail_time=user_fail.login_fail_time)
            return Login(errors=[{"message": "The email or password is incorrect", "field": "email", "code": "USER_10"}])
        if user == 'locked':
            return Login(errors=[{"message": "The account have been locked because login fail over three times.", "field": "email", "code": "USER_11"}])

        user = User.objects.filter(email=username).first()
        if user.is_locked:
            return Login(errors=[{"message": "The account have been locked because login fail over three times.", "field": "email", "code": "USER_11"}])
        user.login_fail_time = 0
        user.save()
        input['email'] = username
        user_device=FCMDevice.objects.filter(user_id=user.id).first()
        if user_device:
            user_device.active=True
            user_device.save()
        return super().mutate(root, info, input)

    @classmethod
    def resolve(cls, root, info, **kwargs):
        return cls(user=info.context.user, status=True)

class ChangePasswordInput(graphene.InputObjectType):
    password = graphene.String(required=True)
    new_password = graphene.String(required=True)
    confirm_password = graphene.String(required=True)

class ChangePassword(graphene.Mutation):
    status = graphene.Boolean(default_value=False)
    errors = graphene.List(ErrorType)

    class Arguments:
        input = ChangePasswordInput(required=True)

    def mutate(root, info, input):
        if info.context.user.is_anonymous:
            return ChangePassword(errors=[{"message": "You do not have permission to perform this action", "field": None, "code": "USER_01"}])

        if not check_password(input["password"], info.context.user.password):
            return ChangePassword(errors=[{"message": "Password does not match", "field": "password", "code": "USER_12"}])

        if len(input["new_password"]) < 6:
            return ChangePassword(errors=[{"message": "Password must be at least 6 characters", "field": "password", "code": "USER_03"}])

        if input["new_password"] != input["confirm_password"]:
            return ChangePassword(errors=[{"message": "Confirm password does not match", "field": "confirm_password", "code": "USER_07"}])

        user = info.context.user
        user.set_password(input["new_password"])
        user.save()

        return ChangePassword(status=True)

class LogoutInput(graphene.InputObjectType):
    token = graphene.String(required=True)
    user = graphene.String(required=True)
    type_device = graphene.String(required=True)
class Logout(graphene.Mutation):
    status = graphene.Boolean(default_value=False)
    token_device=graphene.String()

    class Arguments:
        input=LogoutInput(required=True)
        
    def mutate(root, info, input):
        user=User.objects.filter(id=user)
        if user is not None:
            return Logout(status=False,errors=[{"message":"user does not exists","code":"User_01",'field':'logout'}])
        user_device=FCMDevice.objects.filter(registration_id=input["token"],user_id=user.id).first()
        if user_device:
            user_device.active=False
            user_device.save()
        else:
            FCMDevice.objects.create(registration_id=input["token"],active=False,type=input["type_device"],user_id=user.id)
        return Logout(status=True,token_device=input["type_device"])




class Mutation(graphene.ObjectType):

    auth_login = Login.Field()
    auth_change_password = ChangePassword.Field()

    auth_logout=Logout.Field()

    admin_create_user = AdminCreateUser.Field()
    admin_update_user = AdminUpdateUser.Field()

    auth_forgot_password_token = ForgotPasswordToken.Field()
    auth_forgot_password_verify = ForgotPasswordVerify.Field()
    auth_create_new_password = CreateNewPassword.Field()

    user_change_profile = ChangeProfile.Field()
