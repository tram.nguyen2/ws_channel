import graphene
from graphene_django.debug import DjangoDebug

import tnt.api.schema
from tnt.api.users.query.schema import Query as UsersQuery
from tnt.api.users.mutation.schema import Mutation as UsersMutation
from tnt.api.chat.query.schema import Query as ChatQuery

class Query(UsersQuery,ChatQuery):
    debug = graphene.Field(DjangoDebug, name="_debug")


class Mutation(UsersMutation):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)

