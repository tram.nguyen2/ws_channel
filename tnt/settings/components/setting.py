from firebase_admin import initialize_app


FIREBASE_APP = initialize_app

# GOOGLE_APPLICATION_CREDENTIALS='/path/to/credentials.json'
FILE_UPLOAD_PERMISSIONS = 0o644
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')


# TEMPLATES = {

# 'DIRS': [ BASE_DIR.joinpath('templates') ],

# }

#websocket

#in asgi.py file
ASGI_APPLICATION = "tnt.asgi.application"
CHANNEL_LAYERS = {
 'default': {
 'BACKEND': 'channels.layers.InMemoryChannelLayer',
#  'CONFIG': {
#  'hosts': [('localhost', 6379)],
#  },
 },
}


FCM_DJANGO_SETTINGS = {
     # default: _('FCM Django')
    "APP_VERBOSE_NAME": "AppChat",

    "ONE_DEVICE_PER_USER": False,

    "DELETE_INACTIVE_DEVICES": True,
}



AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
    "graphql_auth.backends.GraphQLAuthBackend",
    "guardian.backends.ObjectPermissionBackend",
    "tnt.api.users.mutation.schema.CustomModelBackend",
]

AUTH_USER_MODEL = "users.User"


STATIC_URL = "/static/"
STATIC_ROOT = "/static/"
MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "media")
CONTEXT_MEDIA = "media"

GRAPHENE = {
    "SCHEMA": "tnt.api.schema.schema",
    "MIDDLEWARE": ["graphql_jwt.middleware.JSONWebTokenMiddleware", "graphene_django.debug.DjangoDebugMiddleware", ],
}
GRAPHQL_JWT = {
    "JWT_ALLOW_ANY_CLASSES": [
        "graphql_auth.relay.VerifyAccount",
        "graphql_auth.relay.ObtainJSONWebToken",
        "graphql_auth.relay.VerifyToken",
        "graphql_auth.relay.RefreshToken",
        "graphql_auth.relay.RevokeToken",
    ],
}


GRAPHQL_AUTH = {
    'LOGIN_ALLOWED_FIELDS': ["email"],
    "REGISTER_MUTATION_FIELDS": ["email"],
}

FILE_UPLOAD_HANDLERS = [
    "django.core.files.uploadhandler.TemporaryFileUploadHandler",
]


SENDGRID_API_KEY = os.environ.get("SENDGRID_API_KEY", "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",)
# EMAIL_BACKEND = "sgbackend.SendGridBackend"
SERVER_URL = os.environ.get("SERVER_URL", "http://127.0.0.1")


REST_FRAMEWORK = {
    "DEFAULT_RENDERER_CLASSES": [
        "rest_framework.renderers.JSONRenderer",
        "rest_framework.renderers.BrowsableAPIRenderer",
    ]
}

RABBITMQ_URL = os.environ.get("RABBITMQ_URL", "amqp://rabbitmq")
