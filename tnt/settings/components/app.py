INSTALLED_APPS += [
    # 3rd app
    "graphene_django",
    "graphql_auth",
    "graphql_jwt.refresh_token.apps.RefreshTokenConfig",
    "django_cleanup",
    "corsheaders",
    "guardian",
    "django_filters",
    "service_objects",
    "xlrd",
    "rest_framework",
    # models
    "tnt.api",
    "tnt.users",
    "channels",
    "tnt.blog",
    "fcm_django"
]
