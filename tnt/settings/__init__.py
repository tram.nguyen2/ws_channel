from split_settings.tools import optional, include
from os import environ

ENV = environ.get('DJANGO_ENV') or 'development'

base_settings = [
    'components/initial.py',
    'components/app.py',
    'components/setting.py',
    'components/email.py',
    'components/database.py',

    # You can even use glob:
    # 'components/*.py'

    # Select the right env:
    'environments/{0}.py'.format(ENV),
    # # Optionally override some settings:
    # optional('environments/development.py'),
]

# Include settings:
include(*base_settings)
